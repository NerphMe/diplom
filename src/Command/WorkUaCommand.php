<?php

namespace App\Command;

use App\Entity\Vacancy;
use App\Entity\WorkUa;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class WorkUaCommand extends Command
{
    protected static $defaultName = 'WorkUa';

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Parsin site for a Vacancy')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {


        for ($i = 1; true; $i++) {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://www.work.ua/ru/jobs-it/?page=' . $i);
            $content = $response->getContent();


            $crawler = new Crawler();
            $crawler->addContent($content);


            /** @var \DOMElement[] $jobNodes */
            $jobNodes = $crawler->filter('#pjax-job-list > div.card-hover > h2 > a');
//            var_dump($jobNodes);die;
//
            var_dump('https://www.work.ua/ru/jobs-it/?page=' . $i);
            var_dump($i . ' ' . count($jobNodes) . '\n');
            if (count($jobNodes) === 0) {
                break;
            }
            foreach ($jobNodes as $jobNode) {

                $job = new Vacancy();
                $job->setVacancy($jobNode->textContent);
                $job->setUrl('http://work.ua'.$jobNode->getAttribute('href'));
                $job->setSalary(random_int(5, 100) * 1000);
                $job->setSiteName('WorkUa');

                $this->entityManager->persist($job);
            }


            if (($i % 6) === 0) {
                $this->entityManager->flush();
            }
        }
        $this->entityManager->flush();

        return 0;

    }
}
