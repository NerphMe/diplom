<?php

namespace App\Command;

use App\Entity\DouUa;
use App\Entity\Vacancy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class Dou extends Command
{
    protected static $defaultName = 'DouUa';

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }
    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 1; true; $i++) {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://jobs.dou.ua/vacancies/?category=PHP'. $i);
            $content = $response->getContent();

            $crawler = new Crawler();
            $crawler->addContent($content);

            /** @var \DOMElement[] $jobNodes */
            $jobNodes = $crawler->filter('#vacancyListId > ul > li > div > div.title > a');

            var_dump('https://jobs.dou.ua/vacancies/?category=PHP' . $i);
            var_dump($i . ' ' . count($jobNodes) . '\n');
            if (count($jobNodes) === 0) {
                break;
            }

            foreach ($jobNodes as $jobNode) {

                $job = new Vacancy();
                $job->setVacancy($jobNode->textContent);
                $job->setUrl('https://jobs.dou.ua' .$jobNode->getAttribute('href'));
                $job->setSalary(random_int(5, 100) * 1000);
                $job->setSiteName('Dou');

                $this->entityManager->persist($job);
            }
            if (($i % 6) === 0) {
                $this->entityManager->flush();
            }

            $this->entityManager->flush();


        }   return 0;
    }
}
