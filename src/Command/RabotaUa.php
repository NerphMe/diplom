<?php

namespace App\Command;

use App\Entity\RobotaUa;
use App\Entity\Vacancy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class RabotaUa extends Command
{
    protected static $defaultName = 'RabotaUa';

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('parsing sie RabotaUa')
            ->addArgument('arg1', InputArgument::OPTIONAL, '')
            ->addOption('option1', null, InputOption::VALUE_NONE, '');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        for ($i = 1; true; $i++) {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://rabota.ua/zapros/it/pg' . $i);
            $content = $response->getContent();

            $crawler = new Crawler();
            $crawler->addContent($content);

            /** @var \DOMElement[] $jobNodes */
            $jobNodes = $crawler->filter('#ctl00_content_vacancyList_gridList > tr > td > article > div.card-body > div.card-main-content > div.common-info > p.card-title > a');

            var_dump('https://rabota.ua/zapros/it/pg' . $i);
            var_dump($i . ' ' . count($jobNodes) . '\n');
            if (count($jobNodes) === 0) {
                break;
            }

            foreach ($jobNodes as $jobNode) {

                $job = new Vacancy();
                $job->setVacancy($jobNode->textContent);
                $job->setUrl('http://rabota.ua'.$jobNode->getAttribute('href'));
                $job->setSiteName('rabota_ua');
                $job->setSalary(random_int(5, 100) * 1000);

                $this->entityManager->persist($job);
            }

            if (($i % 6) === 0) {
                $this->entityManager->flush();
            }
        }
        $this->entityManager->flush();

        return 0;
    }
}
