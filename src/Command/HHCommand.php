<?php

namespace App\Command;

use App\Entity\HH;
use App\Entity\Vacancy;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class HHCommand extends Command
{
    protected static $defaultName = 'HH';

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 1; true; $i++) {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://kiev.hh.ua/search/vacancy?L_is_autosearch=false&area=115&clusters=true&enable_snippets=true&text=it&page=');
            $content = $response->getContent();


            $crawler = new Crawler();
            $crawler->addContent($content);


            /** @var \DOMElement[] $jobNodes */
            $jobNodes = $crawler->filter('div.vacancy-serp > div.vacancy-serp-item > div > div > span > span > span > a');

            var_dump('https://kiev.hh.ua/search/vacancy?L_is_autosearch=false&area=115&clusters=true&enable_snippets=true&text=it&page=1' . $i);
            var_dump($i . ' ' . count($jobNodes) . '\n');
            if (count($jobNodes) === 0) {
                break;
            }
            foreach ($jobNodes as $jobNode) {

                $job = new Vacancy();
                $job->setVacancy($jobNode->textContent);
                $job->setUrl('https://kiev.hh.ua/' . $jobNode->getAttribute('href'));
                $job->setSiteName('HH');
                $job->setSalary(random_int(5, 100) * 1000);

                $this->entityManager->persist($job);
            }
            if (($i % 11) === 0) {
                $this->entityManager->flush();
            }
        }

        $this->entityManager->flush();

        return 0;
    }
}
