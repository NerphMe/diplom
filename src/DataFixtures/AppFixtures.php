<?php

namespace App\DataFixtures;

use App\Entity\Data;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
         $product = new Data();
         $product->setDate(new \DateTime('2011.21.2222'));
         $manager->persist($product);

        $manager->flush();
    }
}
