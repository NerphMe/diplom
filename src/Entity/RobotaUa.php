<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RobotaUaRepository")
 */
class RobotaUa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Vacancy;

    /**
 * @ORM\Column(type="string", length=255)
 */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $site_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVacancy(): ?string
    {
        return $this->Vacancy;
    }

    public function setVacancy(string $Vacancy): self
    {
        $this->Vacancy = $Vacancy;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteName()
    {
        return $this->site_name;
    }

    /**
     * @param mixed $site_name
     */
    public function setSiteName($site_name): void
    {
        $this->site_name = $site_name;
    }

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $salary;

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary): void
    {
        $this->salary = $salary;
    }

}
