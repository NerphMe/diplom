<?php

namespace App\Controller;

use App\Entity\Vacancy;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class VacancyController extends FOSRestController
{
    /**
     * @Rest\Post(path="/getApi", name="get_api")
     * @param Request $request
     * @return JsonResponse
     */
    public function ApiAll(Request $request)
    {
        $content = $request->getContent();
        $jobNames = json_decode($content, true)['jobs'];
        $result = ['labels' => [], 'values' => []];

        foreach ($jobNames as $jobName) {
            $repository = $this->getDoctrine()->getRepository(Vacancy::class);
            $result['labels'][] = $jobName;
            $result['values'][] = $repository->getCountByName($jobName);
        }

        return new JsonResponse($result);
    }

    /**
     * @Rest\Post(path="/getApiSalary", name="get_api_salary")
     * @param Request $request
     * @return JsonResponse
     */
    public function ApiAllSalaries(Request $request)
    {
        $content = $request->getContent();
        $jobNames = json_decode($content, true)['jobs'];
        $result = ['labels' => [], 'values' => []];

        foreach ($jobNames as $key => $jobName) {
            $repository = $this->getDoctrine()->getRepository(Vacancy::class);
            $result['labels'][] = $jobName;
            $result['values'][] = $repository->getAverageSalaryByName($jobName);
        }

        return new JsonResponse($result);
    }

    /**
     * @Rest\Post(path="/getApiMinSalary", name="get_api_min_salary")
     * @param Request $request
     * @return JsonResponse
     */
    public function minimumSalary(Request $request)
    {
        $content = $request->getContent();
        $jobNames = json_decode($content, true)['jobs'];
        $result = ['labels' => [], 'values' => []];

        foreach ($jobNames as $jobName) {
            $repository = $this->getDoctrine()->getRepository(Vacancy::class);
            $result['labels'][] = 'MIN ' . $jobName;
            $result['values'][] = $repository->getMinSalaryByName($jobName);

            $result['labels'][] = 'MAX ' . $jobName;
            $result['values'][] = $repository->getMaxSalaryByName($jobName);
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/jobs/{jobName}", name="jobs_by_job_name")
     */
    public function jobs(string $jobName)
    {

        $repository = $this->getDoctrine()->getRepository(Vacancy::class);
        $resultVacancies = $repository->getJobsByName($jobName);
        $result = [];

        /** @var Vacancy $resultVacancy */
        foreach ($resultVacancies as $resultVacancy) {
            $result[] = ['job_name' => $resultVacancy->getVacancy(), 'url' => $resultVacancy->getUrl()];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/canvas", name="canvas")
     */
    public function graph()
    {
        return $this->render('canvasJs.html.twig');
    }

    /**
     * @Route("/input", name="input")
     */
    public function input()
    {
        return $this->render('input.html.twig');
    }

    /**
     * @Route("/table", name="table")
     */
    public function table()
    {
        $em = $this->getDoctrine()->getManager();
        $table = $em->getRepository(Vacancy::class)->findAll();
        return $this->render('table.html.twig', [
        'tables' => $table]);
    }
}
