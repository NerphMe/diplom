<?php

namespace App\Repository;

use App\Entity\Vrtual;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Vrtual|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vrtual|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vrtual[]    findAll()
 * @method Vrtual[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeadHunterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vrtual::class);
    }

    // /**
    //  * @return Vrtual[] Returns an array of Vrtual objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vrtual
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
