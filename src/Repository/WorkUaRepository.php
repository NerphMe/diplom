<?php

namespace App\Repository;

use App\Entity\WorkUa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WorkUa|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkUa|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkUa[]    findAll()
 * @method WorkUa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkUaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkUa::class);
    }

    // /**
    //  * @return WorkUa[] Returns an array of WorkUa objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WorkUa
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
