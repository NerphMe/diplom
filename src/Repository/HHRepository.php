<?php

namespace App\Repository;

use App\Entity\HH;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HH|null find($id, $lockMode = null, $lockVersion = null)
 * @method HH|null findOneBy(array $criteria, array $orderBy = null)
 * @method HH[]    findAll()
 * @method HH[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HHRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HH::class);
    }

    // /**
    //  * @return HH[] Returns an array of HH objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HH
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
