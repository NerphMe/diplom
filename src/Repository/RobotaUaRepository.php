<?php

namespace App\Repository;

use App\Entity\RobotaUa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RobotaUa|null find($id, $lockMode = null, $lockVersion = null)
 * @method RobotaUa|null findOneBy(array $criteria, array $orderBy = null)
 * @method RobotaUa[]    findAll()
 * @method RobotaUa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RobotaUaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RobotaUa::class);
    }

    // /**
    //  * @return RobotaUa[] Returns an array of RobotaUa objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
//    /**
//     * @param string $searchName
//     * @return string
//     * @throws \Doctrine\ORM\NoResultException
//     * @throws \Doctrine\ORM\NonUniqueResultException
//     */
//    public function getCountByName(string $searchName)
//    {
//        $qb = $this->createQueryBuilder('r');
//
//        $qb
//            ->select('count(r.id)')
//            ->andWhere('lower(r.Vacancy) LIKE :val')
//            ->setParameter('val', '%' . strtolower($searchName) . '%');
//
//        return $qb->getQuery()->getSingleScalarResult();
//    }
}
