<?php

namespace App\Repository;

use App\Entity\Vacancy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Vacancy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vacancy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vacancy[]    findAll()
 * @method Vacancy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VacancyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vacancy::class);
    }



    public function getCountByName(string $searchName)
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->select('count(r.id)')
            ->andWhere('lower(r.vacancy) LIKE :val')
            ->setParameter('val', '%' . mb_strtolower($searchName) . '%');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getJobsByName(string $searchName)
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->andWhere('lower(r.vacancy) LIKE :val')
            ->setParameter('val', '%' . mb_strtolower($searchName) . '%');

        return $qb->getQuery()->getResult();
    }

    public function getAverageSalaryByName(string $searchName)
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->select('sum(r.salary)')
            ->andWhere('lower(r.vacancy) LIKE :val')
            ->setParameter('val', '%' . mb_strtolower($searchName) . '%');

        $sum = $qb->getQuery()->getSingleScalarResult();

        $vacanciesCount = $this->getCountByName($searchName);

        if ($vacanciesCount === 0) {
            return 0;
        }

        return round($sum / $vacanciesCount);
    }
    public function getMinSalaryByName(string $searchName)
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->select('min(r.salary)')
            ->andWhere('lower(r.vacancy) LIKE :val')
            ->setParameter('val', '%' . mb_strtolower($searchName) . '%');


        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getMaxSalaryByName(string $searchName)
    {
        $qb = $this->createQueryBuilder('r');

        $qb
            ->select('max(r.salary)')
            ->andWhere('lower(r.vacancy) LIKE :val')
            ->setParameter('val', '%' . mb_strtolower($searchName) . '%');


        return $qb->getQuery()->getSingleScalarResult();
    }

}
